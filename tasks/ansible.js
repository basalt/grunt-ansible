/**
 * Module with the grunt-foobar plugin
 *
 * @module grunt-foobar
 * @author Basalt AB
 * @license
 * Copyright (c) 2018 Basalt AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

const execSync = require('child_process').execSync;

// Populate the ansible object with configuration from grunt config
//
// First we read from options, then we check for playbook/item specific config.
function readConfig(grunt, config, ansible, name) {
  ansible.flags = grunt.option.flags();

  if (typeof(config) !== 'undefined') {
    if ('options' in config) {
      ansible.config = config['options'];
    }

    if (name in config) {
      ansible.config = config[name];
    }
  }
}

// Run ansible-galaxy
function galaxy(grunt, arg1, arg2) {

  var Ansible = require('./lib/ansibleGalaxy');
  var ansible = new Ansible();

  // if 1 argument it is the name, of two it is type+name
  var type = 'collection';
  var name = '';
  if (typeof(arg2) === 'undefined' && typeof(arg1) !== 'undefined') {
    name = arg1;
  } else if (typeof(arg2 !== 'undefined')) {
    type = arg1;
    name = arg2;
  }

  readConfig(grunt, grunt.config.data.ansible.galaxy, ansible, name);

  return ansible.install(type, name);
}

// Run ansible-playbook
function playbook(grunt, name) {
  var Ansible = require('./lib/ansiblePlaybook');
  var ansible = new Ansible();
  readConfig(grunt, grunt.config.data.ansible.playbooks, ansible, name);
  return ansible.cmd(name);
}

module.exports = function(grunt) {

  grunt.registerTask('ansible', 'Run ansible-playbook.', function(arg1, arg2, arg3) {

    var cmd = '';

    // grunt ansible:galaxy:...
    if (arg1 == 'galaxy') {
      cmd = galaxy(grunt, arg2, arg3);

    // grunt ansible:playbook:...
    } else if (arg1 == 'playbook') {
      cmd = playbook(grunt, arg2);

    // grunt ansible:...
    } else {
      cmd = playbook(grunt, arg1);
    }

    grunt.log.ok(cmd);
    execSync(cmd, {stdio:[0,1,2]});
  });
};
