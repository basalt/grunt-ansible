'use strict';

global.artifact = require('./package.json');
global.artifact.build = process.env.CI_PIPELINE_ID || 'N/A';
global.beans = require('yamljs').load('.beans.yml');
global.semver = require('semver');

module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  require('load-grunt-config')(grunt);
  grunt.config('artifact', global.artifact);
  grunt.config('beans', global.beans);
};
